package com.stpdiron.lab3.beans.managed;

import com.stpdiron.lab3.beans.RawDot;
import com.stpdiron.lab3.beans.mbeans.HitsStatisticsBean;
import com.stpdiron.lab3.beans.mbeans.InputStatisticsBean;
import com.stpdiron.lab3.entities.Dot;
import com.stpdiron.lab3.model.DotDao;
import com.stpdiron.lab3.model.HitDetectionService;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@ApplicationScoped
public class ResultsControllerBean {
    @Inject
    private HitDetectionService detectionService;
    @Inject
    private RawDot currentDot;
    private List<Dot> history;
    private DotDao dotDao;
    private EntityManager entityManager;
    private int score = 0;

    private HitsStatisticsBean hitsStatisticsBean;
    private InputStatisticsBean inputStatisticsBean;

    @PostConstruct
    private void init(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("local");
        entityManager = factory.createEntityManager();
        dotDao = new DotDao(entityManager);
        history = dotDao.getAll();
        for (Dot dot : history) {
            if (dot.isHit())
                score++;
        }
        try {
            registerMBeans();
        } catch (MalformedObjectNameException | NotCompliantMBeanException
                 | InstanceAlreadyExistsException | MBeanRegistrationException e) {
            Logger.getLogger("application").log(Level.WARNING, "Mbeans doesnt work with "+e.toString());
        }
    }

    @PreDestroy
    private void destroy() {
        entityManager.close();
    }

    private void registerMBeans() throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        hitsStatisticsBean = new HitsStatisticsBean(this);
        inputStatisticsBean = new InputStatisticsBean();
        mbs.registerMBean(hitsStatisticsBean,
                new ObjectName("com.stpdiron.lab3.beans.mbeans:type=HitsStatisticsBean"));
        mbs.registerMBean(inputStatisticsBean,
                new ObjectName("com.stpdiron.lab3.beans.mbeans:type=InputStatisticsBean"));
        Logger.getLogger("application").log(Level.FINE, "Mbeans started well");
    }

    public void matchResult() {
        inputStatisticsBean.click();
        Dot dot = detectionService.processDot(currentDot);
        if (dotDao.addDot(dot)) {
            history.add(dot);
            currentDot.setY(0);
            currentDot.setX(0);
            if (dot.isHit())
                score++;
            if (history.size() % 10 == 0)
                hitsStatisticsBean.notify("The number of point is multiple of 10.");
        }
    }


    public RawDot getCurrentDot() {
        return currentDot;
    }

    public void setCurrentDot(RawDot currentDot) {
        this.currentDot = currentDot;
    }

    public List<Dot> getHistory() {
        return history;
    }

    public int getHitsNumber() { return score; }

    public void setHistory(List<Dot> history) {
        this.history = history;
    }
}