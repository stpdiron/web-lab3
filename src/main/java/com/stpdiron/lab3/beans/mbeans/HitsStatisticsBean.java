package com.stpdiron.lab3.beans.mbeans;

import com.stpdiron.lab3.beans.managed.ResultsControllerBean;
import javax.management.*;

public class HitsStatisticsBean implements HitsStatisticsMXBean, NotificationBroadcaster {
    private static final String NOTIFICATION_TYPE = "hits.statistics.notification";
    private long notificationSequence = 0;
    private final ResultsControllerBean controllerBean;
    private final NotificationBroadcasterSupport broadcaster = new NotificationBroadcasterSupport();

    public HitsStatisticsBean(ResultsControllerBean bean) {
        controllerBean = bean;
    }

    @Override
    public int getHitsNumber() {
        return controllerBean.getHitsNumber();
    }

    @Override
    public int getDotsNumber() {
        return controllerBean.getHistory().size();
    }

    public void notify(String text) {
        broadcaster.sendNotification(
                new Notification(
                        NOTIFICATION_TYPE,
                        this,
                        ++notificationSequence,
                        text
                )
        );
    }

    @Override
    public int getMissesNumber() {
        return getDotsNumber() - getHitsNumber();
    }

    @Override
    public void addNotificationListener(NotificationListener listener, NotificationFilter filter, Object handback) throws IllegalArgumentException {
        broadcaster.addNotificationListener(listener, filter, handback);
    }

    @Override
    public void removeNotificationListener(NotificationListener listener) throws ListenerNotFoundException {
        broadcaster.removeNotificationListener(listener);
    }

    @Override
    public MBeanNotificationInfo[] getNotificationInfo() {
        return new MBeanNotificationInfo[] {
                new MBeanNotificationInfo(
                        new String[] { NOTIFICATION_TYPE },
                        Notification.class.getName(),
                        "Hit statistics"
                )
        };
    }
}
