package com.stpdiron.lab3.beans.mbeans;

public interface HitsStatisticsMXBean {
    int getHitsNumber();
    int getDotsNumber();
    int getMissesNumber();
}
