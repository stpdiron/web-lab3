package com.stpdiron.lab3.beans.mbeans;

public interface InputStatisticsMXBean {
    long getMillisBetweenClicksAvg();
}
