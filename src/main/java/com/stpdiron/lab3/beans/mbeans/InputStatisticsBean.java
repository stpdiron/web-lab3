package com.stpdiron.lab3.beans.mbeans;

import java.util.logging.Level;
import java.util.logging.Logger;

public class InputStatisticsBean implements InputStatisticsMXBean {
    private long summaryTimeBetweenClicks = 0;
    private long lastClicked = -1;
    private int clickNumber = 0;

    public void click() {
        if (lastClicked == -1) {
            lastClicked = System.currentTimeMillis();
            return;
        }
        summaryTimeBetweenClicks += (System.currentTimeMillis() - lastClicked);
        lastClicked = System.currentTimeMillis();
        clickNumber++;
    }

    @Override
    public long getMillisBetweenClicksAvg() {
        if (clickNumber == 0)
            return 0;
        return summaryTimeBetweenClicks / clickNumber;
    }
}
